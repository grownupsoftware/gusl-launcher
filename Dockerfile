# syntax=docker/dockerfile:1

FROM openjdk:11-jdk

MAINTAINER Grownup Software Limited

ENV PORT=$port
ENV HOST=$host
ENV CONFIG=$config
ENV LOCATION=$location
ENV ENTITY_SIZE=4096000
ENV CORS_HEADERS="foo"

#WORKDIR /app

RUN mkdir -p /logs
COPY ./build/libs/gusl-launcher.jar /
CMD java -jar gusl-launcher.jar -port $PORT -host $HOST -entitysize $ENTITY_SIZE -corsheaders $CORS_HEADERS -location webapps -war *.war
