package gusl.launcher;

import org.junit.Test;

/**
 * @author dhudson
 */
public class CommandLineTester {

    //@Test
    public void commandLineTest() {
// "-site", "site1.zip,site2.zip",
        String[] args = {"-site", "site1.zip,site2.zip", "-war", "war1.war,war2.war"};

        Launcher launcher = new Launcher();
        launcher.bootstrap(args);

    }

    @Test
    public void siteTest() {
        String[] args = {"-location", "/tmp", "-site", "/Users/dhudson/development/lottomart/casanova-launcher/src/test/resources/site.json"};
        Launcher launcher = new Launcher();
        launcher.bootstrap(args);
    }

    @Test
    public void warTest() {
        String[] args = {"-location", "/tmp", "-war", "/tmp/router.war"};
        Launcher launcher = new Launcher();
        launcher.bootstrap(args);
    }
}
