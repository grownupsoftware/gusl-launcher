package gusl.launcher;

/**
 * @author dhudson on 23/04/2020
 */
public class Resolver {


    public Resolver() {
    }

    public void process() {

        String[] args = {"-location", "/tmp", "-site", "/Users/dhudson/development/lottomart/casanova-launcher/src/test/resources/site_static.json"};
        Launcher launcher = new Launcher();
        launcher.bootstrap(args);

        System.out.println("All done ...");
    }

    public static void main(String[] args) {
        new Resolver().process();
    }
}
