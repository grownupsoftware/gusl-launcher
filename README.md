![Alt text](https://www.googleapis.com/download/storage/v1/b/gusl-images/o/logo.png?generation=1588764693002370&alt=media)
# gusl-launcher

This sub project is the bootstrap for starting nodes.

The system Property Log4jContextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector" will be added automatically to enable Async Logging.

## Arguments

| Argument | Parameter | Optional | Description |
| --- | --- | --- | --- |
| h | none | yes | Display help message|
| -port| port number | default is 8080 | Port to listen on |
| -host | host name | default 'localhost' | host to bind to |
| -location | target extraction location | default 'cwd' | Working directory for war extraction |
| -war | list of war files | yes | list of war files to extract and run as an App Server, this must be the last argument | 
| -site | site config file | yes | site config file, see below |
| -noexpand | none | yes |  Don't expand are wars, just run what is already unpacked on disk |


### Command line 

```
    java -jar gusl-launcher.jar ....
```

### site file 
```json
{
  "name": "Darrens Test Site",
  "web-apps": [
    {
      "context": "/router",
      "path": "router",
      "war": "lm.lotto:lotto-router:war:${Version}",
      "name": "router"
    },
    {
      "context": "/dataservice",
      "path": "router",
      "war": "lm.lotto:lotto-dataservice:war:${Version}",
      "name": "dataservice"
    },
    {
      "context": "/backoffice",
      "path": "backoffice",
      "name": "backoffice",
      "site": "lm.lotto:lm-admin-ui:zip:9.7.2",
      "zip-root": "lm-admin-ui"
    },
    {
      "context": "/player",
      "path": "player",
      "name": "player",
      "site": "lm.lotto:lotto-player-ui:zip:2.52",
      "zip-root": "player"
    }
  ],
  "system-properties": {
    "Version": "2.2.88"
  }
}
```

### System Properties
| Parameter | Description |
| --- | --- |
| LauncherEntitySize | Maximum size of an inbound entity |
| ApplicationLogs | Log directory location |
| ApplicationPort | Port to listen on |
 

###  Docker
change the version number
```shell
  ./gradlew gusl-launcher:assemble
  ./gradlew gusl-launcher:publish
  ./gradlew gusl-launcher:buildImage gusl-launcher:pushImage
```


